# OpenML dataset: ibm-employee-attrition

https://www.openml.org/d/43893

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

IBM Employee Attrition Data 
 The dataset used in the code pattern is supplied by Kaggle and contains HR analytics data of employees
that stay and leave. The types of data include metrics such as education level, job satisfactions, and commmute distance. 
 The dataset was obtained
from https://github.com/IBM/employee-attrition-aif360. 

The dataset is available under the Open Dataset License and the Database Content License.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43893) of an [OpenML dataset](https://www.openml.org/d/43893). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43893/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43893/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43893/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

